Get-CimInstance -Class Win32_LogicalDisk |
    Select-Object -Property Name, FreeSpace
