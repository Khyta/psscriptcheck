Write-Host "Installing PSScriptAnalyzer..."
Install-Module PSScriptAnalyzer -Scope CurrentUser -Confirm:$False -Force

$failures = 0
$files = Get-ChildItem "/builds/Khyta/psscriptcheck/src"
foreach ($f in $files){
    Write-Host -NoNewLine "Checking file $f"
    $result = Invoke-ScriptAnalyzer $f 
    if ($result.Count -gt 0) {
        Write-Host "`t❌ Warnings found:"
        $result | Select-Object | Format-Table 
        $failures += 1
        } else {
        Write-Host "`t✅ No warnings"
    }
}

if ($failures -gt 0) {
    exit $failures 
}
